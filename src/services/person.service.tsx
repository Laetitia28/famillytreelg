// Models
import { Person } from "../models/Person.model";

/**
 * fillPartners
 * @param famillyTree
 */
export const fillPartners = (famillyTree: Person[]): Person[] => {
    famillyTree.map((person: Person) => {
        return person.partners = (famillyTree.filter((p: Person) => {
            return person.children.includes(p.id)
        })
            .map((child: Person) => {
                return child.parents.find((ele: number) => ele !== person.id);
            })
            .filter((el, pos, arr) => el != undefined && arr.indexOf(el) === pos))
            .find(x => x !== undefined);
    });
    return famillyTree;
}
/**
 * getPerson
 * @param idPerson 
 */
export const getPerson = (idPerson: Number, arr: Person[]): Person | undefined => {
    return arr.find((p: Person) => p.id === idPerson)
}
