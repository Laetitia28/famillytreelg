import React, { useEffect, useState } from 'react';

// Mocks
import { provideFullMock } from './mocks/familly-tree-mock';
// Models
import { Person } from './models/Person.model';
// Components
import Pair from './views/Pair/Pair';
// Services
import { fillPartners } from './services/person.service';
// Styles
import { createUseStyles } from 'react-jss';

const useStyles = createUseStyles({
  app: {
    textAlign: 'center',
  }
});

/**
 * App component
 */
const App = () => {
  // Styles
  const classes = useStyles();
  /** FamillyTree   */
  const famillyTree = provideFullMock().familyTree;
  /** topParents   */
  const [topParents, setTopParents] = useState<Person[]>([]);
  /** famillyTreeWithPartner   */
  const [famillyTreeWithPartner, setFamillyTreeWithPartner] = useState<Person[]>([]);

  /**
   * findTopParents
   * @param famillyTree 
   */
  const findTopParents = (famillyTree: Person[]): Person[] => {
    return famillyTree.filter((person: Person) => {
      return person.parents.length === 0 &&
        famillyTree.find(el => el.id === person.partners)!.parents.length === 0
    });
  };

  /**
  * Use effect
  */
  useEffect(() => {
    setFamillyTreeWithPartner(fillPartners(famillyTree));
    setTopParents(findTopParents(fillPartners(famillyTree)))
  }, []);
  
  return (
    <div className={classes.app}>
      <ul className="list-inline list-unstyled ">
        {topParents && topParents.length > 1 ?
          <React.Fragment>
            <li className="list-inline-item" >
              <Pair key={topParents[0].id} parent1={topParents[0]} famillyTree={famillyTreeWithPartner} />
            </li>
          </React.Fragment>
          : null
        }
      </ul>
    </div>
  );
}

export default App;
