import React from 'react';
// Models
import { Person } from '../../models/Person.model';
//Services
import { getPerson } from '../../services/person.service';
// Styles
import { createUseStyles } from 'react-jss';
import { callbackify } from 'util';

const useStyles = createUseStyles({
  icon: {
    paddingRight: 10
  },
  badgeCustum: {
    minWidth: 170
  },
  children: {
    position: "relative",
    verticalAlign: 'top',
    '&:before': {
      content: '""',
      position: 'absolute',
      bottom: "100%",
      right: '50%',
      borderRight: '1px solid #ccc',
      width: '100%',
      height: 10,
    },
  },
  girl: {
    backgroundColor: 'lightpink',
  },
  boy: {
    backgroundColor: 'lightblue',
  },
  partner: {
    position: "relative",
    marginTop: 40,
    borderRadius: 25,
    paddingTop: 10,

  },
  verticalLine: {
    '&:before': {
      content: '""',
      position: 'absolute',
      bottom: "100%",
      right: '50%',
      borderRight: '1px solid #ccc',
      width: '100%',
      height: 40,
    },
  },
  topLine: {
    '&:after': {
      content: '""',
      position: 'absolute',
      bottom: "100%",
      right: '10%',
      borderTop: '1px solid #ccc',
      width: '80%',
      height: 0,
    },
  }
})
/**
 * Pair Props
 */
interface PairProps {
  parent1: Person;
  famillyTree: Person[];
}
const Pair = (props: PairProps) => {
  //Styles
  const classes = useStyles()
  const { parent1, famillyTree } = props;

  /** partner */
  let partner: Person | undefined;
  partner = parent1 && parent1.partners != undefined ? getPerson(parent1.partners, famillyTree) : undefined;
  /** hasChildren */
  const hasChildren = parent1 && parent1.children && parent1.children.length > 0;

  /** isAGirl */
  const isAGirl = (person: Person): boolean => {
    return person && person.gender === "female"
  }

  return (
    <React.Fragment>
      {parent1 ?
        <div>
          <h2>
            <span
              className={`badge ${ classes.badgeCustum } ${ isAGirl(parent1) ? classes.girl : classes.boy }`}>
              <i className={`bi bi-people-fill ${ classes.icon }`}></i>
              {parent1.name}</span>
          </h2>
          {
            partner ? <h2>
              <span className={`badge ${ classes.badgeCustum } ${ isAGirl(partner) ? classes.girl : classes.boy }`}              >
                <i className={`bi bi-people-fill ${ classes.icon }`}></i>
                {partner.name}</span>
            </h2> : null
          }
        </div >
        : null}

      <ul className={`list-inline ${ classes.partner } 
      ${ (hasChildren === true && parent1.children.length > 1) ? classes.topLine : null }
      ${ (hasChildren === true) ? classes.verticalLine : null }`}>
        {hasChildren && parent1.children.map((item: Number) => (
          getPerson(item, famillyTree)! !== undefined ?
            <React.Fragment>
              <li
                className={`list-inline-item ${ classes.children }`}>
                <Pair key={parent1.id} parent1={getPerson(item, famillyTree)!} famillyTree={famillyTree} />
              </li>
            </React.Fragment >
            : null
        ))}</ul>
    </React.Fragment >
  )
};

export default Pair;