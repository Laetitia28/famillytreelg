/**
 * Person interface
 */
export interface Person {
    id: number,
    name: string,
    gender: string,
    children: number[],
    parents: number[],
    partners?: number
}